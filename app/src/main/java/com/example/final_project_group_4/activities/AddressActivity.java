package com.example.final_project_group_4.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import androidx.appcompat.widget.Toolbar;

import com.example.final_project_group_4.R;
import com.example.final_project_group_4.adapter.AddressAdapter;
import com.example.final_project_group_4.models.AddressModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class AddressActivity extends AppCompatActivity implements AddressAdapter.SelectedAddress {

    Button addAddress;
    RecyclerView recyclerView;
    private List<AddressModel> addressModelList;
    private AddressAdapter addressAdapter;
    FirebaseFirestore firestore;
    FirebaseAuth auth;
    Button paymentBtn;
    Toolbar toolbar;
    String mAddress = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);

        toolbar = findViewById(R.id.address_toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        //get data from detailed activity

        toolbar.setNavigationOnClickListener(view -> finish());

        firestore = FirebaseFirestore.getInstance();
        auth = FirebaseAuth.getInstance();

        recyclerView = findViewById(R.id.address_recycler);

        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        addressModelList = new ArrayList<>();
        addressAdapter = new AddressAdapter(getApplicationContext(), addressModelList, this);
        recyclerView.setAdapter(addressAdapter);

        firestore.collection("CurrentUser").document(Objects.requireNonNull(auth.getCurrentUser()).getUid())
                .collection("Address").get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                for (DocumentSnapshot doc : Objects.requireNonNull(task.getResult()).getDocuments()) {

                    AddressModel addressModel = doc.toObject(AddressModel.class);
                    addressModelList.add(addressModel);
                    addressAdapter.notifyDataSetChanged();

                }
            }
        });

        paymentBtn = findViewById(R.id.payment_btn);

        paymentBtn.setOnClickListener(view -> {
            //startActivity(new Intent(AddressActivity.this, PaymentActivity.class));

            double amount = getIntent().getDoubleExtra("item", 0);

            Intent intent = new Intent(AddressActivity.this, PaymentActivity.class);


            intent.putExtra("amount", amount);

            startActivity(intent);
        });

        addAddress = findViewById(R.id.add_address_btn);

        addAddress.setOnClickListener(view -> startActivity(new Intent(AddressActivity.this, AddAddressActivity.class)));
    }

    @Override
    public void setAddress(String address) {
        mAddress = address;
    }
}