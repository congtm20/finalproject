package com.example.final_project_group_4.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.final_project_group_4.R;
import com.example.final_project_group_4.activities.DetailedActivity;
import com.example.final_project_group_4.activities.MainActivity;
import com.example.final_project_group_4.activities.OnBoardingActivity;
import com.example.final_project_group_4.activities.RegitrationActivity;
import com.example.final_project_group_4.models.Product;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import java.util.List;

public class NewProductsAdapter extends RecyclerView.Adapter<NewProductsAdapter.ViewHolder> {

    private Context context;
    private List<Product> list;

    public NewProductsAdapter(Context context, List<Product> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.new_products, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Product model = list.get(position);
        Glide.with(context).load(list.get(position).getImg_url()).into(holder.newImg);
        holder.newName.setText(list.get(position).getName());
        holder.newPrice.setText(String.valueOf(list.get(position).getPrice()));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailedActivity.class);
                intent.putExtra("detailed", model);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView newImg;
        TextView newName, newPrice;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            newImg = itemView.findViewById(R.id.new_img);
            newName = itemView.findViewById(R.id.new_product_name);
            newPrice = itemView.findViewById(R.id.new_price);
        }
    }

    public static class LoginActivity extends AppCompatActivity {

        EditText name, email, password;
        private FirebaseAuth auth;
        SharedPreferences sharedPreferences;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_login);

            //getSupportActionBar().hide();
            auth = FirebaseAuth.getInstance();
            email = findViewById(R.id.email);
            password = findViewById(R.id.password);
            sharedPreferences = getSharedPreferences("onBoardingScreen", MODE_PRIVATE);
            boolean isFirstTime = sharedPreferences.getBoolean("firstTime", true);
            if (isFirstTime) {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean("firstTime", false);
                editor.commit();
                Intent intent = new Intent(LoginActivity.this, OnBoardingActivity.class);
                startActivity(intent);
                finish();
            }
        }

        public void signIn(View view) {
            String userEmail = email.getText().toString();
            String userPassword = password.getText().toString();
            if (TextUtils.isEmpty(userEmail)) {
                Toast.makeText(this, "Email is not empty!", Toast.LENGTH_SHORT).show();
                return;
            }
            if (TextUtils.isEmpty(userPassword)) {
                Toast.makeText(this, "Password is not empty!", Toast.LENGTH_SHORT).show();
                return;
            }
            if (userPassword.length() < 6) {
                Toast.makeText(this, "Must have min 6 character!", Toast.LENGTH_SHORT).show();
                return;
            }
            auth.signInWithEmailAndPassword(userEmail, userPassword)
                    .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(LoginActivity.this, "Login Successfully!",
                                        Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                            } else {
                                Toast.makeText(LoginActivity.this, "Login Failed!" + task.getException(),
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

        }

        public void signUp(View view) {
            startActivity(new Intent(LoginActivity.this, RegitrationActivity.class));
        }
    }
}
